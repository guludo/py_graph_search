#!/bin/python

"""
Missionaries and cannibals

Author: Gustavo Sousa <gu_ludo@yahoo.com.br>
"""

import py_graph_search
from py_graph_search import *

class State(py_graph_search.State):
    LEFT = 0
    RIGHT = 1

    domains = {
        'm': set((0, 1, 2, 3)),
        'c': set((0, 1, 2, 3)),
        'b': set((LEFT, RIGHT)),
        'xy' : set(((0, 0), (0, 1), (1, 0), (1, 1), (0, 2), (2, 0)))
    }

    def __init__(self, m, c, b, x, y):
        self.m = int(m)
        self.c = int(c)
        self.b = int(b)
        self.x = int(x)
        self.y = int(y)

    def evaluate(self):
        return self.m + self.c

    def left_m(self):
        n = self.m
        if self.b == State.LEFT:
            n += self.x
        return n

    def left_c(self):
        n = self.c
        if self.b == State.LEFT:
            n += self.y
        return n

    def right_m(self):
        n = 3 - self.m
        if self.b == State.LEFT:
            n -= self.x
        return n

    def right_c(self):
        n = 3 - self.c
        if self.b == State.LEFT:
            n -= self.y
        return n

    def is_valid(self):
        d = {
            'm': self.m,
            'c': self.c,
            'b': self.b,
            'xy' : (self.x, self.y)
        }

        for k in d:
            if d[k] not in State.domains[k]:
                return False

        xy_is_zero = (self.x, self.y) == (0, 0)
        """
        one_bank_is_empty = (0, 0) in ((self.m, self.c),
                                       (self.right_m(), self.right_c()))
        """
        one_bank_is_empty = (self.m, self.c) in ((0, 0), (3, 3))
        if xy_is_zero and not one_bank_is_empty:
            return False

        if self.m + self.x > 3:
            return False

        if self.c + self.y > 3:
            return False

        if 0 < self.left_m() < self.left_c():
            return False

        if 0 < self.right_m() < self.right_c():
            return False

        return True

    def on_board(self, new_x, new_y):
        """
        Operation to change number of people onboard.
        """
        if (self.x, self.y) == (new_x, new_y):
            return None

        if self.b == State.LEFT:
            return State(self.m + self.x - new_x,
                         self.c + self.y - new_y,
                         self.b,
                         new_x,
                         new_y)
        else:
            return State(self.m, self.c, self.b, new_x, new_y)

    def transport(self):
        # if boat is empty
        if (self.x, self.y) == (0, 0):
            return None

        return State(self.m,
                     self.c,
                     State.RIGHT if self.b == State.LEFT else State.LEFT,
                     self.x,
                     self.y)

    def expand(self):
        """
        Return an iterable of tuples in the form:
            (operation, new_state)

            - operation represents the operation and the parameters used to
              generate the new state
            - new_state: the resulting state
        """
        l = []

        for x, y in State.domains['xy']:
            s = self.on_board(x, y)
            if s and s.is_valid():
                operation = 'E(' + str(x) + ', ' +  str(y) + ')'
                l.append((operation, s))

        s = self.transport()
        if s and s.is_valid():
            operation = 'T( )'
            l.append((operation, s))

        return tuple(l)

    def __iter__(self):
        return iter((self.m, self.c, self.b, self.x, self.y))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __str__(self):
        m, c, b, x, y = self
        b = 'E' if b == State.LEFT else 'D'
        return str((m, c, b, x, y))

    def __repr__(self):
        return str(self)


s0 = State(3, 3, State.LEFT, 0, 0)
goal = State(0, 0, State.RIGHT, 0, 0)
problem = Problem(initial_state=s0, goal=goal)

path = depth_first(problem)
print("Depth first")
print(path)

print()

path = breadth_first(problem)
print("Breadth first")
print(path)

print()

path = uniform_cost(problem)
print("Uniform cost")
print(path)

path = hill_climbing(problem)
print("Hill climbing")
print(path)

path = best_first(problem)
print("Best first")
print(path)

path =a_star(problem)
print("A*")
print(path)
