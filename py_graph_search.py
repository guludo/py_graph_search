#!/bin/python

"""
Author: Gustavo Sousa <gu_ludo@yahoo.com.br>
"""

class State:
    def expand(self):
        raise NotImplementedError("expand must be implemented")

    def __iter__(self):
        raise NotImplementedError("__iter__ must be implemented")

    def cost_from(self, parent_state):
        return 1

    def evaluate(self):
        return 1

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __str__(self):
        return str(tuple(self))

    def __repr__(self):
        return str(self)

class Node:
    def __init__(self, state, parent=None, operation=None):
        self.parent = parent
        self.operation = operation
        self.state = state
        self.cost = 0
        if parent:
            self.cost = parent.cost + state.cost_from(parent.state)

class Problem:
    def __init__(self, initial_state, goal=None):
        self.initial_state = initial_state
        self.goal = goal

    def state_is_goal(self, state):
        if callable(self.goal):
            return self.goal(state)
        return state == self.goal

def graph_search(problem, queue_nodes=None):
    """
    Perform a graph search.
    """
    initial_state = problem.initial_state
    root = Node(initial_state)
    queue = [root]
    closed_set = set()

    while len(queue) > 0:
        n = queue.pop(0)
        s = n.state

        if problem.state_is_goal(s):
            path = [(n.operation, n.state)]
            while n.parent:
                n = n.parent
                path.insert(0, (n.operation, n.state))
            return tuple(path)

        closed_set.add(tuple(s))
        queue_addition = []
        for e in s.expand():
            if type(e) == tuple:
                operation, state = e
            else:
                operation = None
                state = e

            if tuple(state) in closed_set:
                continue

            queue_addition.append(Node(state, parent=n, operation=operation))

        queue_nodes(queue, queue_addition)

    return None

def _depth_first_queue_nodes(queue, nodes):
    l = list(nodes)
    l.reverse()
    for n in l:
        queue.insert(0, n)

def depth_first(problem):
    return graph_search(problem,
                        queue_nodes=_depth_first_queue_nodes)

def _breadth_first_queue_nodes(queue, nodes):
    queue.extend(nodes)

def breadth_first(problem):
    return graph_search(problem,
                        queue_nodes=_breadth_first_queue_nodes)

def _sort_cost_key(n):
    return n.cost

def _uniform_cost_queue_nodes(queue, nodes):
    queue.extend(nodes)
    queue.sort(key=_sort_cost_key)

def uniform_cost(problem):
    return graph_search(problem,
                        queue_nodes=_uniform_cost_queue_nodes)

def _hill_climbing_queue_nodes(queue, nodes):
    min_cost = -float('inf')
    best_node = None
    for n in nodes:
        cost = n.state.evaluate()
        if cost < min_cost:
            best_node = n
            min_cost = cost
    if best_node:
        queue.append(best_node)

def hill_climbing(problem):
    return graph_search(problem,
                        queue_nodes=_hill_climbing_queue_nodes)

def _sort_evaluate_key(n):
    return n.state.evaluate()

def _best_first_queue_nodes(queue, nodes):
    queue.extend(nodes)
    queue.sort(key=_sort_evaluate_key)

def best_first(problem):
    return graph_search(problem,
                        queue_nodes=_best_first_queue_nodes)

def _a_star_sort_key(n):
    return n.cost + n.state.evaluate()

def _a_star_queue_nodes(queue, nodes):
    queue.extend(nodes)
    queue.sort(key=_a_star_sort_key)

def a_star(problem):
    return graph_search(problem,
                        queue_nodes=_a_star_queue_nodes)
